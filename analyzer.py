import pickle
import networkx as nx
from collections import deque
import sys
import os, pdb

debug = False  

def decomp_groups(otpt):

    D = otpt['DAG']
    D1 = convert_dag(D)

    #get decomposition groups:
    grp = []

    for i in otpt['results']:
        try:
            i[1].remove('s')
        except ValueError:
            pass 
        grp.append(i[1])
    return grp

def convert_dag(D):

    D1 = nx.DiGraph()

    for node in D.nodes():
        #split nodes into 0-jobs and 1-jobs
        #0-job as wt = 1, p = 0 ; 1-job is oppose
        D1.add_node(str(node) + "_0", w = 1, p = 0)
        D1.add_node(str(node) + "_1", w = 0 ,p = 1)
        #create an edge between them
        D1.add_edge(str(node)+"_0",str(node)+"_1")

    #now convert the edges:
    for edge in D.edges():
        D1.add_edge(str(edge[0]) + "_1", str(edge[1]) + "_0")

    return D1

def mwc_scheduler(otpt):

    ''' schedules via topological sort of subcomponents''' 

    grp = decomp_groups(otpt)
    D1 = convert_dag(otpt['DAG'])

    order = []

    for i in grp:
        D_sub = D1.subgraph(i)
        ord = nx.topological_sort(D_sub)
        order.extend(ord)

    return order


def order_deconverter(order):

    new_order = []
    order = deque(order)
    while len(order) > 0:
        u = order.popleft()
        if u[-1] == '1':
           new_order.append(int(u[0:get_int_from_string(u)]))

                
    return new_order

def decomp_greedy(otpt):

    grp = decomp_groups(otpt)
    D = otpt['DAG'].copy()
    ord = []

    for i in grp:
        temp = order_deconverter(i)
        #print temp, D.nodes()
        temp_sched = dynamic_greedy_sub(D,temp)
        #print temp_sched
        ord.extend(temp_sched)
        D.remove_nodes_from(temp_sched)
        
    return ord 
    
    
def get_int_from_string(u):
    i = 0
    while u[i] != '_':
        i += 1

    return i 
    

def compute_area(D,sched):

    D_c = D.copy()
    sched = deque(sched)
    E = []
    A = 0 
    #create initial elegibility schedule 
    for v in D_c.nodes():
        if D_c.predecessors(v) == []:
            E.append(v)

    A += len(E)
    while len(sched) != 0:
        u = sched.popleft()
        #print u 
        try:
            E.remove(u)
        except ValueError:
            #print u
            #print E
            #print sched 
            pass
        
        for v in D.neighbors(u):
            if D_c.predecessors(v) == [u]:
                E.append(v)
        D_c.remove_node(u)
        #print E
        A += len(E)
        

    return A



def find_repeats(order):

    freq = {}
    for i in order:
        if i in freq:
            freq[i] += freq[i]
        else:
            freq[i] = 1


    offenders = []
    for i in freq:
        if freq[i] > 1:
             offenders.append(i)

    return offenders


def reorganize_equeue(D,E):

    E_out = deque()
    for e in E:
        E_out.append((e[0],get_yield(D,e[0])))

    E_out = list(E_out) 
    E_out.sort(key = lambda tup : tup[1])
    E_out.reverse()
    E_out = deque(E_out) 
    return E_out

def get_yield(D,e):
    yld = 0 
    for v in D.neighbors(e):
        if D.predecessors(v) == [e]:
            yld += 1

    return yld


def dynamic_greedy(D):

    D_c = D.copy()
    E = deque()
    ord = []
    #create initial elegibility schedule 
    for v in D_c.nodes():
        if D_c.predecessors(v) == []:
            E.append((v,get_yield(D_c,v)))

    E = reorganize_equeue(D_c,E)

    while len(E) != 0:
        e = E.popleft()
        ord.append(e[0])
        for v in D_c.neighbors(e[0]):
            if D_c.predecessors(v) == [e[0]]:
                E.append((v,get_yield(D_c,v)))
        
        E = reorganize_equeue(D_c,E)
        D_c.remove_node(e[0])

    
    return ord


def dynamic_greedy_sub(D,nbunch):
    
    D_c = D.copy()
    E = deque()
    ord = []

    for v in nbunch:
        if D_c.predecessors(v) ==  []:
            E.append((v,get_yield(D_c,v)))

    E = reorganize_equeue(D_c,E)

    while len(E) != 0:
        e = E.popleft()
        ord.append(e[0])
        for v in D_c.neighbors(e[0]):
            if D_c.predecessors(v) == [e[0]] and v in nbunch:
                E.append((v,get_yield(D_c,v)))

        E = reorganize_equeue(D_c,E)
        D_c.remove_node(e[0])

    return ord

