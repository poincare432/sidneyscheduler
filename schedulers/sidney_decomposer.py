import networkx as nx
import pickle,os,sys,time,pdb
import dag_analyzer as an


class graph_decomposer(object):


    def __init__(self,orig_dag,verbose=False):
        self.orig_dag = orig_dag
        self.decomp_sets = []
        self.construct_01_dag()
        self.verbose = verbose 



    def construct_01_dag(self):

        D = nx.DiGraph()
        [D.add_nodes_from([repr(i) + "_0",repr(i) + "_1"]) for i in self.orig_dag.nodes()]
        [D.add_edge(repr(i) + "_0", repr(i) + "_1") for i in self.orig_dag.nodes()]
        [D.add_edge(repr(e[0]) + "_1", repr(e[1]) + "_0") for e in self.orig_dag.edges()]
        self.bin_dag = D



    def decompose(self):

        
        lambdas = create_lambda_vec(self.bin_dag)
        k = self.bin_search(lambdas,0,0)

        while k!= None:
            (S,f,flow_dict) = self.decompose_step(lambdas[k])
            self.decomp_sets.append((lambdas[k],list(S)))
            self.bin_dag.remove_nodes_from(S)
            if self.verbose: print "Removed:"
            if self.verbose: print S
            lambdas = create_lambda_vec(self.bin_dag)
            k = self.bin_search(lambdas,0,0)
            
            

        if k == None:
            self.decomp_sets.append((0,self.bin_dag.nodes()))
            
        
        

    def bin_search(self,lambdas,i,last):


        if i >= len(lambdas):
            i = len(lambdas) -1

        if len(lambdas) == 0:
            return None

        try:
            (S, f, flow_dict) = self.decompose_step(lambdas[i])

        except IndexError:
            print "Error " + str(i)
        
        W = lambdas[i] * sum([1 for j in self.bin_dag.nodes() if j[-1] == '0'])

        if self.verbose: 
            print "W is " + repr(W)
            print "f is " + repr(f)
            print "i is " + repr(i)
            print "last is " + repr(last)
            print "S is:"
            print S 
        
        if f <= W and abs(last-i) <= 1 and S != set([]):
            return i

        elif f<=W and S == set([]):
            return self.bin_search(lambdas,i + pow(2,i-last),i)

        elif i >= len(lambdas):
            if f > W:
                return None
            else:
                return self.bin_search(lambdas,last+1,last+1)

        elif f <= W and abs(last-i) > 1 and S != set([]):
                return self.bin_search(lambdas,last+1,last+1) 
        else:
            if f > W:
                return self.bin_search(lambdas,i + pow(2,i-last),i)



    def decompose_step(self,lamb):

        if self.verbose:
            print "In decompose_step"
            print "size of dag is :"
            print self.bin_dag.number_of_nodes()
        
        dag = self.bin_dag.reverse()
        dag.add_nodes_from(['s','t'])

        # here it is!
        [dag.add_edge('s',v,capacity = lamb) for v in self.bin_dag.nodes() if v[-1] == '0']
        [dag.add_edge(v,'t',capacity = 1) for v in self.bin_dag.nodes() if v[-1] == '1'] 

        (flow,flow_dict) = nx.ford_fulkerson(dag,'s','t')
        
        for e in find_cut(dag,flow_dict):
            dag.remove_edge(e[0],e[1])

        group_to_remove = descendants(dag,'s')
        del(dag) 
        return (group_to_remove, flow, flow_dict)
    
        
def descendants(G, source):
    """Return all nodes reachable from `source` in G.

    Parameters
    ----------
    G : NetworkX DiGraph
    source : node in G

    Returns
    -------
    des : set()
       The descendants of source in G
    """
    if not G.has_node(source):
        raise nx.NetworkXError("The node %s is not in the graph." % source)
    des = set(nx.shortest_path_length(G, source=source).keys()) - set([source])
    return des


def find_cut(dag,flow_dict):

    cut = []
    for e in dag.edges():
        if 'capacity' in dag[e[0]][e[1]]:
            if flow_dict[e[0]][e[1]] == dag[e[0]][e[1]]['capacity']:
                cut.append(e)

    return cut  


def create_lambda_vec(dag):
    jobs1  = jobs0 = 0;
    for node in dag.nodes():
        if node[-1] == '0': jobs0 += 1
        elif node[-1] == '1': jobs1 += 1

    j1 = [i for i in xrange(jobs1)]
    j0 = [i for i in xrange(1,jobs0)]

    lambdas = [1.0*i/j for i in j1 for j in j0]
    l = set(lambdas)
    lambdas = list(l)
    lambdas.sort()

    lambdas_sparse = reduce_lams(lambdas,dag.size())
    return lambdas_sparse

def reduce_lams(lams,dag_size):

    prev = 0.0
    lam2 = []
    
    for l in lams:
        l_temp = l
        if l > prev + 1/(2.0*dag_size):
            lam2.append(l)
        prev = l_temp 
    return lam2


def convert_dag(D):

    D1 = nx.DiGraph()

    for node in D.nodes():
        #split nodes into 0-jobs and 1-jobs
        #0-job as wt = 1, p = 0 ; 1-job is oppose
        D1.add_node(str(node) + "_0", w = 1, p = 0)
        D1.add_node(str(node) + "_1", w = 0 ,p = 1)
        #create an edge between them
        D1.add_edge(str(node)+"_0",str(node)+"_1")

    #now convert the edges:
    for edge in D.edges():
        D1.add_edge(str(edge[0]) + "_1", str(edge[1]) + "_0")

    return D1


    


    
