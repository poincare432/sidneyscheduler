import sys,os,pickle
import networkx as nx
import sydney_decomposer as syd

sys.path.append("/users/poincare432/scheduling/simulations/simulations/decomp")
sys.path.append("/users/poincare432/scheduling/simulations/simulations/pyjyinterface/lego")
import ek_decomp as dec
import dag_analyzer as an


class nx_working_graph(object):

    def __init__(self,file_source):
        f_load = open(file_source,'r')
        temp_graph = pickle.load(f_load)
        f_load.close()
        
        self.dag = build_nx_dag(temp_graph['graph'])
        self.size = temp_graph['size']
        self.aospd_sched = temp_graph['schedule']
        self.seed = temp_graph['seed']
        self.generationMode = temp_graph['generationMode']
        
        
    def build_nx_dag(self,temp_graph):

        D = nx.DiGraph()
        D.add_nodes_from(temp_graph[0])
        D.add_edges_from(temp_graph[1])
        
        return D 
        

    def decompose(self):
        self.decomposer = syd.graph_decomposer(self.dag)
        self.decomposer.decompose()
        self.decomposition = self.decomposer.decomp_sets
        
        

    def schedule_decomp_greedy(self):
        '''wrapper for decomp_greedy from an'''

        input_dict = {}
        input_dict['DAG'] = self.dag
        input_dict['results'] = self.decomposition

        self.decomp_sched = an.decomp_greedy(input_dict)


    def schedule_dynamic_greedy(self):

        self.dynamic_sched = an.dynamic_greedy(self.dag) 


    def compute_areas(self):

        self.decomp_area = an.compute_area(self.dag,self.decomp_sched)
        self.aospd_area = an.compute_area(self.dag,self.aospd_sched)
        self.greedy_area = an.compute_area(self.dag,self.dynamic_sched)
        


    

    

        

        

    
    



    
