#! /usr/bin/env python

import os,sys
import time
import sidney_scheduler_lite as ssl
import networkx as nx 


def main():

    verbose = True 
    print "Demo 1, creating a DAG"
    print "Nodes: [0,1,2,3,4]"
    print "Edges: [(0,1),(1,2),(2,3),(3,4)]"
    print "Invoking build_dag_from_list:"

    dag_example = [[0,1,2,3,4],[(0,1),(1,2),(2,3),(3,4)]]
    dag_nx = ssl.build_dag_from_list(dag_example)

    print "DAG stored as: "
    print "Nodes:"
    print dag_nx.nodes
    print dag_nx.nodes()
    print "Edges:"
    print dag_nx.edges
    print dag_nx.edges()

    print "Demo 2: Saving DAG to file ./example.dag"
    print "Invoking save_dag_to_file"
    ssl.save_dag_to_file(dag_nx,'./example.dag')
    print "DAG present in:"
    print os.listdir('.')
    
    print "Demo 3: Loading DAG from file"
    print "Deleting dag_nx example"
    del(dag_nx)
    print "Reloading DAG from file"
    dag_nx=ssl.build_dag_from_file('./example.dag')
    print "DAG consists of:"
    print "Nodes"
    print dag_nx.nodes()
    print "Edges"
    print dag_nx.edges()

    print "Computing Sidney schedule order"
    print "VERBOSE set to True, default is false"

    (a,b) = ssl.compute_sidney_schedule(dag_nx)

    print "Sidney schedule for dag_nx is:"
    print a
    print "Area for schedule is:"
    print b 
    

    print "Loading larger DAG"
    dag_rand = ssl.build_dag_from_file('./random_example.dag')
    print "DAG nodes are:"
    print dag_rand.nodes()
    print "DAG edges are:"
    print dag_rand.edges()

    verbose =False 
    print "Computing Sidney schedule for DAG"
    print "Verbose set to False"
    (a,b) = ssl.compute_sidney_schedule(dag_rand,verbose)
    print "Sidney schedule is:"
    print a
    print "Area is:"
    print b
    


if __name__=='__main__':
    sys.exit(main())
