#! /usr/bin/env python2.7

import sys,os
import networkx as nx
import pickle
import schedulers.sidney_decomposer as sidney
import analyzer as an
#from decomp2 import  

def build_dag_from_list(dag_list):
    '''
    dag_list is a list with two elements
    element 0 is a list of nodes (integers)
    element 1 is a list of edges (pairs of integers)
    for example:
    dag_list = [[0,1,2],[(0,1),(1,2),(2,0)]
    will create a directed cycle with 3 nodes

    dag_nx is a networkx.DiGraph object with the corresponding
    node and edge topology
    '''
    D = nx.DiGraph()
    D.add_nodes_from(dag_list[0])
    D.add_edges_from(dag_list[1]) 
    
    return D


def save_dag_to_file(dag_nx,filename):

    try:
        with open(filename,'w') as f:
            pickle.dump(dag_nx,f)
        return 0 
    except IOError:
        return 1 
        


def build_dag_from_file(file_name):
    '''
    reads a file saved by the function save_dag_to_file
    and returns a networkx.DiGraph() object
    '''
    try:
        with open(file_name,'r') as f:
            dag_nx = pickle.load(f) 
    
        return dag_nx

    except IOError:

        return None


def compute_sidney_schedule(dag_nx,verbose=False):

    '''
    input is a networkx.DiGraph object with non-empty node and edge sets
    output is a tuple consisting of 1) a list of nodes in sidney order and 2)
    the area that corresponds to this schedule 
    '''
    
    sidney_graph = sidney.graph_decomposer(dag_nx,verbose)
    sidney_graph.decompose() #call the sidney decomposition subroutine
    dag_nodes_partition = sidney_graph.decomp_sets #partitioned nodes from sidney algorithm
    
    sidney_sched = compute_sidney_greedy(dag_nx,dag_nodes_partition)
    sidney_area = an.compute_area(dag_nx,sidney_sched)

    return (sidney_sched,sidney_area)
    

def compute_sidney_greedy(dag_nx,node_partition):

    '''
    TODO: function description
    '''

    input_dict = {}
    input_dict['DAG'] = dag_nx
    input_dict['results'] = node_partition

    sched = an.decomp_greedy(input_dict)

    return sched


    
